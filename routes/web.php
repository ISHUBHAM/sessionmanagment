<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::view('/login','login');
Route::post('/login','login@index');


Route::get('/profile', function () {


    return view('profile');
});

Route::get('/logout', function () {


	session()->forget('data');
	return redirect('login');


});




Route::group(['middleware'=>['customAuth']],function(){

	Route::view('/profile','profile');

	Route::get('/', function () {
    return view('welcome');
});

});



// route for paytm package


Route::get('/payment','PaytmController@pay');
Route::post('/payment/status', 'PaytmController@paymentCallback');








